#!/bin/bash

set -e
set -x

image_base_name=$1  # Must be the name of the directory

docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
if [[ $CI_COMMIT_TAG == "" ]]; then
    # The commit is not tagged, use the first 7 chars of git commit hash
    image_version=${CI_COMMIT_SHA:0:7}
    DNAME="$CI_REGISTRY_IMAGE/ci-test:$image_base_name-$image_version"
else
    # The commit is tagged, convert tag version properly:v1.0.1 -> "1.0.1"
    image_version=${CI_COMMIT_TAG:1}
    DNAME="$CI_REGISTRY_IMAGE:$image_base_name-$image_version"
fi

docker build -t "$DNAME" ./$image_base_name

docker push "$DNAME"
