FROM ubuntu:18.04
MAINTAINER Ondřej Čertík <ondrej@certik.us>

# Get sudo working
RUN apt-get update && DEBIAN_FRONTEND=noninteractive \
    apt-get install -yq --no-install-recommends \
        sudo ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && echo "swuser ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/swuser \
    && echo "Defaults env_reset" >> /etc/sudoers.d/swuser \
    && echo "Defaults env_keep = \"http_proxy https_proxy no_proxy\"" >> /etc/sudoers.d/swuser \
    && hash -r

# Switch to a user:
RUN groupadd -r swuser -g 1000 && \
    mkdir /home/swuser && \
    useradd -u 1000 -r -g swuser -d /home/swuser -s /sbin/nologin \
         -c "Docker image user" swuser && \
    chown -R swuser:swuser /home/swuser && \
    echo "swuser:swuser" | chpasswd && \
    adduser swuser sudo
WORKDIR /home/swuser
USER swuser


# Install Conda and project packages
RUN sudo apt-get update && DEBIAN_FRONTEND=noninteractive \
    sudo apt-get install -yq --no-install-recommends \
        wget bzip2 gcc g++ gfortran gcc-multilib make && \
    sudo apt-get clean && \
    sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    wget --no-check-certificate https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh && \
    bash miniconda.sh -b -p $HOME/conda_root && \
    rm miniconda.sh && \
    export PATH="$HOME/conda_root/bin:$PATH" && \
    conda config --set always_yes yes --set changeps1 no && \
    conda info -a && \
    conda update -q -n root conda && \
    conda install -c conda-forge python=3.8 pytest cmake llvmdev=11.0.1 && \
    conda clean --all && \
    hash -r
